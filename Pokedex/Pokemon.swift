//
//  Pokemon.swift
//  Pokedex
//
//  Created by Tejas Hedly on 02/07/16.
//  Copyright © 2016 Tejas Hedly. All rights reserved.
//

import Foundation

class Pokemon {
    
    private var _name : String
    private var _pokedexId : Int
    
    
    var Name: String {
        return _name
    }
    
    var PokedexId : Int {
        return _pokedexId
    }
    
    
    init (name :String, pokedexId: Int) {
        _name = name
        _pokedexId = pokedexId
    }
}
