//
//  PokeDetailsVC.swift
//  Pokedex
//
//  Created by Tejas Hedly on 02/07/16.
//  Copyright © 2016 Tejas Hedly. All rights reserved.
//

import UIKit

class PokeDetailsVC: UIViewController {

    var pokemon : Pokemon!
    
    
    @IBOutlet weak var pokemonNameLbl: UILabel!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.pokemonNameLbl.text = pokemon.Name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
           }
    

  
}
