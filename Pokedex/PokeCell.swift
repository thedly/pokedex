//
//  PokeCell.swift
//  Pokedex
//
//  Created by Tejas Hedly on 02/07/16.
//  Copyright © 2016 Tejas Hedly. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
    @IBOutlet weak var thumbImage : UIImageView!
    @IBOutlet weak var nameLbl : UILabel!
    
    var pokemon : Pokemon!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 5.0
    }
    
    func configureCell(private _pokemen : Pokemon) {
        self.pokemon = _pokemen
        nameLbl.text = _pokemen.Name.capitalizedString
        thumbImage.image = UIImage(named: "\(self.pokemon.PokedexId)")
    }
}
